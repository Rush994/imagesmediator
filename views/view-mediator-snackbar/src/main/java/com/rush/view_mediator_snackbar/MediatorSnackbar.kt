package com.rush.view_mediator_snackbar

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.graphics.drawable.DrawableCompat
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.rush.core_presentation.extension.colorChanger

class MediatorSnackbar private constructor(
    parent: ViewGroup,
    content: MediatorSnackbarView
) : BaseTransientBottomBar<MediatorSnackbar>(parent, content, content) {
    companion object {
        fun make(
            parent: ViewGroup,
            duration: Int,
            type: MediatorSnackbarType
        ): MediatorSnackbar {
            var backgroundColorId = 0
            var foregroundMessageId = 0
            when (type) {
                MediatorSnackbarType.INTERNET_NO -> {
                    backgroundColorId = android.R.color.holo_red_light
                    foregroundMessageId = R.string.snackbar_no_connection
                }
                MediatorSnackbarType.INTERNET -> {
                    backgroundColorId = android.R.color.holo_green_light
                    foregroundMessageId = R.string.snackbar_connection_established
                }
            }

            val customView = (LayoutInflater.from(parent.context).inflate(
                R.layout.view_mediator_snackbar,
                parent,
                false
            ) as MediatorSnackbarView).also {
                it.message.text = parent.context.resources.getString(foregroundMessageId)
            }
            return MediatorSnackbar(parent, customView).also {
                it.duration = duration
            }.also {
                it.view.background?.let { snackBackground ->
                    val wrappedBackground = DrawableCompat.wrap(snackBackground.mutate())
                    DrawableCompat.setTintList(
                        wrappedBackground,
                        ColorStateList.valueOf(parent.context.colorChanger(backgroundColorId))
                    )
                }
            }
        }
    }

    enum class MediatorSnackbarType(val value: String) {
        INTERNET_NO("INTERNET_NO"), INTERNET("INTERNET");
        companion object {
            private val map = values().associateBy(MediatorSnackbarType::value)
            fun fromValue(type: String) = map[type]
        }
    }
}