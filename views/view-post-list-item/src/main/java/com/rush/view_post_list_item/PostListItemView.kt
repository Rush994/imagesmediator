package com.rush.view_post_list_item

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Paint
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.rush.core_presentation.model.PostItemModel
import com.rush.view_post_list_item.databinding.ViewPostListItemBinding

class PostListItemView : ConstraintLayout {

    companion object {
        private const val ANIM_DURATION = 150
    }

    //region Init
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context, attrs)
    }

    var paint: Paint? = null
    private lateinit var binding: ViewPostListItemBinding

    private fun init(context: Context, attrs: AttributeSet?) {
        binding = ViewPostListItemBinding.inflate(LayoutInflater.from(context))
        addView(binding.root)
        ConstraintSet().apply {
            clone(this@PostListItemView)
            match(binding.root, this@PostListItemView)
        }

        paint = Paint().apply {
            isAntiAlias = true
            isDither = true
            style = Paint.Style.FILL
        }

        attrs?.let { set ->
            val typedArray =
                context.obtainStyledAttributes(set, R.styleable.PostListItemView, 0, 0)
            typedArray.use {
                isTapAnimated = getBoolean(
                    R.styleable.PostListItemView_tapAnimated,
                    false
                )
            }
        }
    }
    //endregion

    private var touchState = 0
    private var downAnim: ValueAnimator? = null
    private var upAnim: ValueAnimator? = null
    private var startCardWidth: Int = 0
    private var startCardHeight: Int = 0
    private var startPostImageWidth: Int = 0
    private var startPostImageHeight: Int = 0
    private var isTapAnimated = false


    var onTapUpMethod: (() -> Unit)? = null

    //region Animation on touch
    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (isTapAnimated) {
            if (event.action == MotionEvent.ACTION_DOWN) {
                touchState = MotionEvent.ACTION_DOWN
                startDownAnimation()
                return true
            } else if (event.action == MotionEvent.ACTION_CANCEL) {
                downAnim = null
                startUpAnimation()
                return true
            } else if (event.action == MotionEvent.ACTION_UP &&
                touchState == MotionEvent.ACTION_DOWN
            ) {
                touchState = MotionEvent.ACTION_UP
                startUpAnimation()
                onTapUpMethod?.invoke()
                return true
            }
        } else {
            if (event.action == MotionEvent.ACTION_DOWN) {
                touchState = MotionEvent.ACTION_DOWN
                return true
            } else if (event.action == MotionEvent.ACTION_CANCEL) {
                touchState = 0
                return true
            } else if (event.action == MotionEvent.ACTION_UP &&
                touchState == MotionEvent.ACTION_DOWN
            ) {
                touchState = MotionEvent.ACTION_UP
                onTapUpMethod?.invoke()
                return true
            }
        }
        performClick()
        return super.onTouchEvent(event)
    }

    override fun performClick(): Boolean {
        return super.performClick()
    }

    private fun startDownAnimation() {
        if (downAnim != null) return
        downAnim = ValueAnimator.ofFloat(1f, .9f).apply {
            duration = ANIM_DURATION.toLong()
            interpolator = DecelerateInterpolator()
            addUpdateListener {
                onAnimateSizeChange(it)
            }
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    downAnim = null
                    if (touchState == MotionEvent.ACTION_UP) {
                        startUpAnimation()
                    }
                }
            })
        }
        downAnim?.start()
    }

    private fun startUpAnimation() {
        if (upAnim != null || downAnim != null) return
        upAnim = ValueAnimator.ofFloat(.9f, 1f).apply {
            duration = ANIM_DURATION.toLong()
            interpolator = DecelerateInterpolator()
            addUpdateListener {
                onAnimateSizeChange(it)
            }
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    upAnim = null
                }
            })

        }
        upAnim?.start()
    }

    private fun onAnimateSizeChange(valueAnimator: ValueAnimator) {
        val `val` = valueAnimator.animatedValue as Float
        val newCardHeight = startCardHeight * `val`
        val newCardWidth = startCardWidth * `val`
        val newImageHeight = startPostImageHeight * `val`
        val newImageWidth = startPostImageWidth * `val`

        with(binding) {
            cvPostItemCard.layoutParams.also { params ->
                params.height = newCardHeight.toInt()
                params.width = newCardWidth.toInt()
            }.also { params ->
                cvPostItemCard.layoutParams = params
            }
            ivPostImage.layoutParams.also { params ->
                params.height = newImageHeight.toInt()
                params.width = newImageWidth.toInt()
            }.also { params ->
                ivPostImage.layoutParams = params
            }
        }
    }
    //endregion

    //region Extensions
    private fun ConstraintSet.match(view: View, parentView: View) {
        this.connect(view.id, ConstraintSet.TOP, parentView.id, ConstraintSet.TOP)
        this.connect(view.id, ConstraintSet.START, parentView.id, ConstraintSet.START)
        this.connect(view.id, ConstraintSet.END, parentView.id, ConstraintSet.END)
        this.connect(view.id, ConstraintSet.BOTTOM, parentView.id, ConstraintSet.BOTTOM)
    }

    private fun TypedArray.use(block: TypedArray.() -> Unit) {
        try {
            block()
        } finally {
            this.recycle()
        }
    }
    //endregion

    fun bind(item: PostItemModel) {
        with(binding) {
            if (item.mediaCount == null)
                item.mediaCount = 1
            if (item.mediaCount != 1) {
                with(tvMediasCount)
                {
                    text = item.mediaCount.toString()
                    isVisible = true
                }
            } else {
                tvMediasCount.isVisible = false
            }
            with(tvPostTitle) {
                text = item.title
            }
            Glide.with(context)
                .load("https://i.imgur.com/${item.mediaId}.jpg")
                .centerCrop()
                .placeholder(R.drawable.ic_default_item_post_stub)
                .into(ivPostImage)
                .getSize { width, height ->

                    startPostImageHeight = height
                    startPostImageWidth = resources.getDimension(R.dimen.size_42x).toInt()
                    startCardWidth = cvPostItemCard.width
                    startCardHeight = cvPostItemCard.height

                    flContentContainer.layoutParams.let {
                        it.width = cvPostItemCard.width
                        it.height = cvPostItemCard.height
                    }
                }
        }
    }
}