package com.rush.feature_post.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.rush.core_presentation.base.BaseFragment
import com.rush.feature_post.presentation.PostViewModel
import com.rush.feature_post.databinding.FragmentPostBinding
import com.rush.feature_post.di.FeaturePostComponent
import javax.inject.Inject
import javax.inject.Named

class PostFragment : BaseFragment<FragmentPostBinding>() {

    @Inject
    lateinit var viewModel: PostViewModel

    @Inject
    @Named(MAIN_ADAPTER)
    lateinit var mainLayoutManager: LinearLayoutManager

    @Inject
    @Named(TAGS_MANAGER)
    lateinit var tagsLayoutManager: LinearLayoutManager

    //region Init fragment
    private lateinit var binding: FragmentPostBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPostBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun inject() {
        FeaturePostComponent.inject(this)
    }

    companion object {
        const val MAIN_ADAPTER = "MAIN_ADAPTER"
        const val TAGS_MANAGER = "TAGS_ADAPTER"
    }
}