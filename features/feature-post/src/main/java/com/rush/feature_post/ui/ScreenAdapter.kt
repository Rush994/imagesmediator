package com.rush.feature_post.ui

import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import com.rush.core_presentation.model.PostContents

class ScreenAdapter: AsyncListDifferDelegationAdapter<PostContents>(Companion) {

    private companion object : DiffUtil.ItemCallback<PostContents>() {
        override fun areItemsTheSame(
            oldItem: PostContents,
            newItem: PostContents
        ): Boolean = oldItem == newItem

        override fun areContentsTheSame(
            oldItem: PostContents,
            newItem: PostContents
        ): Boolean = oldItem == newItem
    }
}