package com.rush.feature_post.presentation

import com.rush.core_domain.interactor.PostsInteractor
import com.rush.core_presentation.base.BaseViewModel
import javax.inject.Inject

class PostViewModel@Inject constructor(
    private val postsInteractor: PostsInteractor
): BaseViewModel() {

}