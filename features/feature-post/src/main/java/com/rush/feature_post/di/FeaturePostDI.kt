package com.rush.feature_post.di

import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rush.core_data.di.CoreDataComponent
import com.rush.core_domain.interactor.PostsInteractor
import com.rush.core_presentation.di.Injector
import com.rush.core_presentation.di.ViewModelKey
import com.rush.feature_post.ui.PostFragment
import com.rush.feature_post.presentation.PostViewModel
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Named
import javax.inject.Scope

@Scope
annotation class FeaturePostsScope

@FeaturePostsScope
@Component(
    modules = [FeaturePostModule::class],
    dependencies = [CoreDataComponent::class]
)
interface FeaturePostComponent : Injector<PostFragment> {

    @Component.Builder
    interface Builder{
        @BindsInstance
        fun bind(target: PostFragment): Builder
        fun coreDataComponent(component: CoreDataComponent): Builder
        fun build(): FeaturePostComponent
    }
    companion object {
        fun inject(target: PostFragment){
            val component = DaggerFeaturePostComponent.builder()
                .bind(target)
                .coreDataComponent(CoreDataComponent.build())
                .build()
            component.inject(target)
        }
    }
}

@Module
class FeaturePostModule {
    @FeaturePostsScope
    @Provides
    @[IntoMap ViewModelKey(PostViewModel::class)]
    fun provideViewModel(postsInteractor: PostsInteractor): ViewModel =
        PostViewModel(postsInteractor)

    @Provides
    @Named(PostFragment.MAIN_ADAPTER)
    fun provideLayoutManagerMain(fragment: PostFragment): LinearLayoutManager =
        LinearLayoutManager(fragment.requireContext(), RecyclerView.VERTICAL, false)

    @Provides
    @Named(PostFragment.TAGS_MANAGER)
    fun provideLayoutManagerTags(fragment: PostFragment): LinearLayoutManager =
        LinearLayoutManager(fragment.requireContext(), RecyclerView.HORIZONTAL, false)
}