package com.rush.feature_posts_cat.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.doOnPreDraw
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.rush.core_presentation.action.PostsListActions
import com.rush.core_presentation.base.BaseFragment
import com.rush.core_presentation.base.Constants
import com.rush.core_presentation.extension.launchOnStart
import com.rush.core_presentation.type.CategoryTypes
import com.rush.feature_posts_cat.databinding.FragmentPostsCatBinding
import com.rush.feature_posts_cat.di.FeaturePostsCatComponent
import com.rush.feature_posts_cat.presentation.PostsCatViewModel
import com.rush.feature_posts_cat.presentation.adapter.PostsRecyclerAdapter
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

class PostsCatFragment : BaseFragment<FragmentPostsCatBinding>() {

    //region Init fragment
    private lateinit var binding: FragmentPostsCatBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPostsCatBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun inject() {
        FeaturePostsCatComponent.inject(this)
    }
    //endregion

    @Inject
    lateinit var viewModel: PostsCatViewModel
    @Inject
    lateinit var postsAdapter: PostsRecyclerAdapter

    @Inject
    lateinit var gridLayoutManager: StaggeredGridLayoutManager
    private var lastVisibleItem: Int = 0
    private var currentCategoryType = CategoryTypes.Default

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.doOnPreDraw {
            initUI()
            initData()
        }
    }

    private fun initUI() {
        with(binding) {
            srlPosts.setOnRefreshListener {
                viewModel.handleAction(PostsListActions.FetchNextPortion(isRefresh = true))
            }
            fabUpward.setOnClickListener {
                rvPosts.smoothScrollToPosition(0)
            }
            with(rvPosts) {
                setOnScrollChangeListener { _, _, _, _, _ ->
                    var item = IntArray(2)
                    item = gridLayoutManager.findLastVisibleItemPositions(item)
                    lastVisibleItem = item[0]
                    showUpwardFab()
                    viewModel.handleAction(
                        com.rush.core_presentation.action.PostsListActions.DisplayFetchedPortion(
                            totalItemCount = gridLayoutManager.itemCount,
                            lastVisibleItem = lastVisibleItem
                        )
                    )
                }
                layoutManager = gridLayoutManager
                adapter = postsAdapter
            }
        }
    }

    private fun initData() {
        initStateListeners()
        currentCategoryType = CategoryTypes.Cat
        viewModel.handleAction(PostsListActions.FirstFetchList(currentCategoryType))
    }

    private fun initStateListeners() {
        with(viewModel) {
            postList.onEach { result ->
                postsAdapter.submitList(result)
                binding.srlPosts.isRefreshing = false
            }.launchOnStart(lifecycleScope)
            progressState.onEach { result ->
                with(binding) {
                    val visibilityChanger: (Boolean) -> Unit = { value ->
                        pbPosts.isVisible = value
                        rvPosts.isVisible = !value
                    }
                    if (result.isProgress && postsAdapter.currentList.isNotEmpty()) {
                        srlPosts.isRefreshing = false
                    } else {
                        visibilityChanger(result.isProgress)
                    }
                }
            }.launchOnStart(lifecycleScope)
        }
    }

    private fun showUpwardFab() {
        with(binding) {
            fabUpward.isVisible = lastVisibleItem > Constants.THRESHOLD_RECYCLER_VISIBLE_ITEMS
        }
    }
}