package com.rush.feature_posts_cat.presentation

import android.annotation.SuppressLint
import com.rush.core_domain.entity.toPresentation
import com.rush.core_domain.interactor.PostsInteractor
import com.rush.core_presentation.action.PostsListActions
import com.rush.core_presentation.base.BaseViewModel
import com.rush.core_presentation.base.Constants
import com.rush.core_presentation.extension.performOnIO
import com.rush.core_presentation.model.PostItemModel
import com.rush.core_presentation.state.ProgressBarState
import com.rush.core_presentation.type.CategoryTypes
import io.reactivex.rxjava3.kotlin.subscribeBy
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import timber.log.Timber
import javax.inject.Inject

class PostsCatViewModel @Inject constructor(
    private val postsInteractor: PostsInteractor
): BaseViewModel() {
    private val _postList = MutableStateFlow<List<PostItemModel>>(listOf())
    val postList = _postList.asStateFlow()

    private val _progressState = MutableStateFlow(ProgressBarState())
    val progressState = _progressState.asStateFlow()

    private var postsType: CategoryTypes = CategoryTypes.Default
    private var lastLoadedPage = 0
    private var isLoading = false

    fun handleAction(action: PostsListActions) =
        when (action) {
            is PostsListActions.FirstFetchList -> {
                lastLoadedPage = 0
                postsType = action.type
                initObserver()
                fetchPosts()
            }
            is PostsListActions.FetchNextPortion -> {
                if (action.isRefresh)
                    lastLoadedPage = 0
                fetchPosts()
            }
            is PostsListActions.DisplayFetchedPortion -> {
                displayFetchedPortion(action.totalItemCount, action.lastVisibleItem)
            }
        }

    private fun fetchPosts() {
        Timber.d("Fetching ${++lastLoadedPage} of $postsType category")
        fetchNextPortion()
    }

    private fun initObserver() =
        postsInteractor.observePosts(postsType.value)
            .performOnIO()
            .map { it.toPresentation() }
            .doOnNext { isLoading = false }
            .subscribeBy(onNext = {
                if (it.isNotEmpty()) {
                    _postList.value = it
                    _progressState.value = ProgressBarState(
                        isProgress = false
                    )
                    updateLoading(false)
                }
            }, onError = {
                Timber.d(it.toString())
            }).autoDispose()

    private fun fetchNextPortion() =
        postsInteractor.fetchPosts(page = lastLoadedPage, postType = postsType.value)
            .performOnIO()
            .doOnSubscribe {
                if (lastLoadedPage == 1)
                    _progressState.value = ProgressBarState()
            }
            .subscribeBy(
                onComplete = {
                    Timber.d("$lastLoadedPage of $postsType category is fetched")
                },
                onError = {
                    Timber.d(it.toString())
                }
            ).autoDispose()

    @SuppressLint("TimberArgCount")
    private fun displayFetchedPortion(totalItemCount: Int, lastVisibleItem: Int) {
        if (!isLoading && totalItemCount <= (lastVisibleItem + Constants.THRESHOLD_RECYCLER_LOADER)) {
            updateLoading(true)
            handleAction(PostsListActions.FetchNextPortion())
            Timber.d("Load new list of ${postsType.value}", Constants.MEDIATOR_COMMON_TAG)
        }
    }

    private fun updateLoading(value: Boolean) {
        isLoading = value
    }

    fun requireLoading() = isLoading
}