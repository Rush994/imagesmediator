package com.rush.feature_posts_cat.di

import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.rush.core_data.di.CoreDataComponent
import com.rush.core_domain.interactor.PostsInteractor
import com.rush.core_presentation.di.Injector
import com.rush.core_presentation.di.ViewModelKey
import com.rush.feature_posts_cat.presentation.PostsCatViewModel
import com.rush.feature_posts_cat.ui.PostsCatFragment
import com.rush.feature_posts_cat.presentation.adapter.PostsRecyclerAdapter
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Scope

@Scope
annotation class FeaturePostsCatScope

@FeaturePostsCatScope
@Component(
    modules = [FeaturePostsCatModule::class],
    dependencies = [CoreDataComponent::class]
)
interface FeaturePostsCatComponent : Injector<PostsCatFragment> {

    @Component.Builder
    interface Builder{
        @BindsInstance
        fun bind(target: PostsCatFragment): Builder
        fun coreDataComponent(component: CoreDataComponent): Builder
        fun build(): FeaturePostsCatComponent
    }
    companion object {
        fun inject(target: PostsCatFragment){
            val component = DaggerFeaturePostsCatComponent.builder()
                .bind(target)
                .coreDataComponent(CoreDataComponent.build())
                .build()
            component.inject(target)
        }
    }
}

@Module
class FeaturePostsCatModule {
    @FeaturePostsCatScope
    @Provides
    @[IntoMap ViewModelKey(PostsCatViewModel::class)]
    fun provideViewModel(postsInteractor: PostsInteractor): ViewModel =
        PostsCatViewModel(postsInteractor)

    @Provides
    fun providePostsAdapter(): PostsRecyclerAdapter =
        PostsRecyclerAdapter()

    @Provides
    fun provideStaggeredManager(): StaggeredGridLayoutManager =
        StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)
}