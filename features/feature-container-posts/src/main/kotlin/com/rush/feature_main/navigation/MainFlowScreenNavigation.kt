package com.rush.feature_main.navigation

import com.github.terrakok.cicerone.androidx.FragmentScreen
import com.rush.feature_main.ui.PostsContainerFragment

object MainFlowScreenNavigation {
    fun getFragment() = FragmentScreen {
        PostsContainerFragment()
    }
}