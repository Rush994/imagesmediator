package com.rush.feature_main.presentation.common

import com.github.terrakok.cicerone.Router

interface RouterProvider {
    val router: Router
}