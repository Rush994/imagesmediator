package com.rush.feature_main.di.module

import android.content.IntentFilter
import android.net.ConnectivityManager
import androidx.lifecycle.ViewModel
import com.rush.core_presentation.di.ViewModelKey
import com.rush.core_presentation.provider.ResourceProvider
import com.rush.core_presentation.provider.ResourceProviderImpl
import com.rush.core_presentation.receiver.NetworkStateReceiver
import com.rush.feature_main.navigation.router.MainFlowRouter
import com.rush.feature_main.presentation.PostsContainerViewModel
import com.rush.feature_main.ui.PostsContainerFragment
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class FeatureContainerPostsModule {

    @Provides
    @[IntoMap ViewModelKey(PostsContainerViewModel::class)]
    fun provideViewModel(
        resourceProvider: ResourceProvider
    ): ViewModel =
        PostsContainerViewModel(resourceProvider)

    @Provides
    fun provideNetworkStateReceiver(fragment: PostsContainerFragment): NetworkStateReceiver =
        NetworkStateReceiver(fragment.requireContext()).also {
            it.addListener(fragment)
            fragment.requireContext().registerReceiver(
                it, IntentFilter(
                    ConnectivityManager.CONNECTIVITY_ACTION
                )
            )
        }

    @Provides
    fun provideResourceProvider(fragment: PostsContainerFragment): ResourceProvider =
        ResourceProviderImpl(fragment.requireContext())
}