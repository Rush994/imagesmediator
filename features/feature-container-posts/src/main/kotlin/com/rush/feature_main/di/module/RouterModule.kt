package com.rush.feature_main.di.module

import com.rush.feature_main.di.FeatureMainFlowScope
import com.rush.feature_main.navigation.router.MainFlowRouter
import com.rush.feature_main.navigation.router.MainFlowRouterImpl
import dagger.Binds
import dagger.Module

@Module
interface RouterModule {
    @FeatureMainFlowScope
    @Binds
    fun bindMainContainerRouter(impl: MainFlowRouterImpl): MainFlowRouter
}