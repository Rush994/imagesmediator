package com.rush.feature_main.presentation.common

interface BackButtonListener {
    fun onBackPressed(): Boolean
}