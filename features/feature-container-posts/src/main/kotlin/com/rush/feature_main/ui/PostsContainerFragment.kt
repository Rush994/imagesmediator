package com.rush.feature_main.ui

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.ashokvarma.bottomnavigation.BottomNavigationBar
import com.ashokvarma.bottomnavigation.BottomNavigationItem
import com.google.android.material.snackbar.Snackbar
import com.rush.core_presentation.base.BaseFragment
import com.rush.core_presentation.extension.launchOnStart
import com.rush.core_presentation.receiver.NetworkStateReceiver
import com.rush.feature_main.R
import com.rush.feature_main.databinding.FragmentMainBinding
import com.rush.feature_main.di.FeatureContainerPostsComponent
import com.rush.feature_main.presentation.PostsContainerViewModel
import com.rush.feature_main.presentation.PostsContainerActions
import com.rush.view_mediator_snackbar.MediatorSnackbar
import kotlinx.coroutines.flow.onEach
import java.net.UnknownHostException
import javax.inject.Inject


class PostsContainerFragment : BaseFragment<FragmentMainBinding>(),
    NetworkStateReceiver.NetworkStateReceiverListener {

    //region Init fragment
    private lateinit var binding: FragmentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun inject() {
        FeatureContainerPostsComponent.inject(this)
    }
    //endregion

    @Inject
    lateinit var viewModel: PostsContainerViewModel

    @Inject
    lateinit var networkStateReceiver: NetworkStateReceiver

    private var snackbarNoConnect: MediatorSnackbar? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        initViews()
        initData()
        if (savedInstanceState == null) {
            binding.bnbPostsLists.selectTab(0, true)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_posts_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.action_search -> {
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    override fun onDestroyView() {
        super.onDestroyView()
        requireContext().unregisterReceiver(networkStateReceiver)
    }

    override fun onNetworkAvailable() {
        viewModel.handleAction(PostsContainerActions.ThrowableAction(UnknownHostException(), false))
    }

    override fun onNetworkUnavailable() {
        viewModel.handleAction(PostsContainerActions.ThrowableAction(UnknownHostException(), true))
    }

    private fun initViews() {
        with(binding) {
            bnbPostsLists
                .addItem(BottomNavigationItem(R.drawable.ic_posts_tab_top, R.string.tab_top))
                .addItem(BottomNavigationItem(R.drawable.ic_posts_tab_cat, R.string.tab_cat))
                .addItem(BottomNavigationItem(R.drawable.ic_posts_tab_dog, R.string.tab_dog))
                .initialise()
            bnbPostsLists.setTabSelectedListener(tabSelectedListener)
        }
    }

    private fun initData() {
        viewModel.onTabSelected = ::selectTab
        initSateListeners()
    }

    private val tabSelectedListener = object : BottomNavigationBar.OnTabSelectedListener {
        override fun onTabSelected(position: Int) {
            viewModel.onSelectTab(position)
            binding.bnbPostsLists.selectTab(position, false)
        }

        override fun onTabUnselected(position: Int) {}

        override fun onTabReselected(position: Int) {
            onTabSelected(position)
        }
    }

    private fun selectTab(tabLabel: String) {
        viewModel.fragmentTagMap[tabLabel]?.let { fragmentTitlePair ->
            setToolbarTitle(fragmentTitlePair.second)
            var currentFragment: Fragment? = null
            childFragmentManager.fragments.let { fragmentList ->
                for (item in fragmentList) {
                    if (item.isVisible) {
                        currentFragment = item
                        break
                    }
                }
            }
            val newFragment = childFragmentManager.findFragmentByTag(tabLabel)
            if (currentFragment != null && newFragment != null && currentFragment === newFragment) return

            childFragmentManager.beginTransaction().also { transaction ->
                if (newFragment == null) {
                    transaction.add(
                        R.id.flContainer,
                        fragmentTitlePair.first.java.newInstance(),
                        tabLabel
                    )
                }
                currentFragment?.let { fragment ->
                    transaction.hide(fragment)
                }
                newFragment?.let { fragment ->
                    transaction.show(fragment)
                }
                transaction.commitNow()
            }
        }
    }

    private fun setToolbarTitle(tabLabel: String) {
        binding.postsToolbar.apply {
            title = tabLabel
        }
    }

    private fun initSateListeners() {
        viewModel.snackbarState.onEach { result ->
            when (result) {
                MediatorSnackbar.MediatorSnackbarType.INTERNET_NO -> {
                    snackbarNoConnect = MediatorSnackbar.make(
                        binding.clPostsContainer,
                        Snackbar.LENGTH_INDEFINITE,
                        MediatorSnackbar.MediatorSnackbarType.INTERNET_NO
                    ).also {
                        it.anchorView = binding.bnbPostsLists
                    }
                    snackbarNoConnect?.show()
                }
                MediatorSnackbar.MediatorSnackbarType.INTERNET -> {
                    snackbarNoConnect?.let {
                        MediatorSnackbar.make(
                            binding.clPostsContainer,
                            Snackbar.LENGTH_SHORT,
                            MediatorSnackbar.MediatorSnackbarType.INTERNET
                        ).also {
                            it.anchorView = binding.bnbPostsLists
                        }.show()
                    }
                    snackbarNoConnect = null
                }
            }
        }.launchOnStart(lifecycleScope)
    }
}
