package com.rush.feature_main.navigation.router

interface MainFlowRouter {

    fun navigateToPost()

    fun navigateToSearch()

    fun back()
}