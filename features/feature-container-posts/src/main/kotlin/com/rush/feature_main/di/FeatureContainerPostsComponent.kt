package com.rush.feature_main.di

import com.rush.core_navigation.di.CoreNavigationComponent
import com.rush.core_presentation.di.Injector
import com.rush.feature_main.di.module.FeatureContainerPostsModule
import com.rush.feature_main.di.module.RouterModule
import com.rush.feature_main.ui.PostsContainerFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Scope

@Scope
@Retention
annotation class FeatureMainFlowScope

@FeatureMainFlowScope
@Component(
    modules = [
        RouterModule::class,
        FeatureContainerPostsModule::class],
    dependencies = [CoreNavigationComponent::class]
)
interface FeatureContainerPostsComponent : Injector<PostsContainerFragment> {

    @Component.Builder
    interface Builder{
        @BindsInstance
        fun bind(target: PostsContainerFragment): Builder
        fun coreNavigationComponent(component: CoreNavigationComponent): Builder
        fun build(): FeatureContainerPostsComponent
    }
    companion object {
        private fun build(target: PostsContainerFragment) =
            DaggerFeatureContainerPostsComponent.builder()
                .bind(target)
                .coreNavigationComponent(CoreNavigationComponent.build())
                .build()

        fun inject(target: PostsContainerFragment){
            build(target).inject(target)
        }
    }
}

