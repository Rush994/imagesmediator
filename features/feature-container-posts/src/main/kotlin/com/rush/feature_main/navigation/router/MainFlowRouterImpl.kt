package com.rush.feature_main.navigation.router

import com.github.terrakok.cicerone.Router
import javax.inject.Inject

class MainFlowRouterImpl @Inject constructor(
    private val appRouter: Router
) : MainFlowRouter {

    override fun navigateToPost() {
        TODO("Not yet implemented")
    }

    override fun navigateToSearch() {
        TODO("Not yet implemented")
    }

    override fun back() {
        appRouter.exit()
    }
}