package com.rush.feature_main.presentation

import androidx.fragment.app.Fragment
import com.rush.core_presentation.base.BaseViewModel
import com.rush.core_presentation.provider.ResourceProvider
import com.rush.feature_main.R
import com.rush.feature_posts_cat.ui.PostsCatFragment
import com.rush.feature_posts_dog.ui.PostsDogFragment
import com.rush.feature_posts_top.ui.PostsTopFragment
import com.rush.view_mediator_snackbar.MediatorSnackbar
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import timber.log.Timber
import java.net.UnknownHostException
import javax.inject.Inject
import kotlin.reflect.KClass

class PostsContainerViewModel @Inject constructor(
    resourceProvider: ResourceProvider
): BaseViewModel() {

    companion object{
        const val TOP_TAB = "TOP_TAB"
        const val CAT_TAB = "CAT_TAB"
        const val DOG_TAB = "DOG_TAB"
    }

    private val _snackbarState = MutableStateFlow(MediatorSnackbar.MediatorSnackbarType.INTERNET)
    val snackbarState = _snackbarState.asStateFlow()

    var fragmentTagMap: MutableMap<String, Pair<KClass<out Fragment>, String>> = mutableMapOf()
    var onTabSelected: ((String) -> Unit)? = null

    init {
        fragmentTagMap[TOP_TAB] = Pair(PostsTopFragment::class, resourceProvider.getString(R.string.tab_top))
        fragmentTagMap[CAT_TAB] = Pair(PostsCatFragment::class, resourceProvider.getString(R.string.tab_cat))
        fragmentTagMap[DOG_TAB] = Pair(PostsDogFragment::class, resourceProvider.getString(R.string.tab_dog))
    }

    fun onSelectTab(position: Int){
        when (position) {
            0 -> onTabSelected?.invoke(TOP_TAB)
            1 -> onTabSelected?.invoke(CAT_TAB)
            2 -> onTabSelected?.invoke(DOG_TAB)
        }
    }

    fun handleAction(action: PostsContainerActions) =
        when (action) {
            is PostsContainerActions.ThrowableAction -> {
                when (action.throwable) {
                    is UnknownHostException ->
                        when (action.isEnabled) {
                            true -> _snackbarState.value = MediatorSnackbar.MediatorSnackbarType.INTERNET_NO
                            false -> _snackbarState.value = MediatorSnackbar.MediatorSnackbarType.INTERNET
                        }
                    else -> Timber.d(action.throwable.toString())
                }
            }
        }
}