package com.rush.feature_main.presentation

sealed class PostsContainerActions{
    data class ThrowableAction(
        val throwable: Throwable,
        val isEnabled: Boolean = true
    ): PostsContainerActions()
}
