package com.rush.feature_posts_dog.di

import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.rush.core_data.di.CoreDataComponent
import com.rush.core_domain.interactor.PostsInteractor
import com.rush.core_presentation.di.Injector
import com.rush.core_presentation.di.ViewModelKey
import com.rush.feature_posts_dog.ui.PostsDogFragment
import com.rush.feature_posts_dog.presentation.PostsDogViewModel
import com.rush.feature_posts_dog.presentation.adapter.PostsRecyclerAdapter
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Scope

@Scope
annotation class FeaturePostsDogScope

@FeaturePostsDogScope
@Component(
    modules = [FeaturePostsDogModule::class],
    dependencies = [CoreDataComponent::class]
)
interface FeaturePostsDogComponent : Injector<PostsDogFragment> {

    @Component.Builder
    interface Builder{
        @BindsInstance
        fun bind(target: PostsDogFragment): Builder
        fun coreDataComponent(component: CoreDataComponent): Builder
        fun build(): FeaturePostsDogComponent
    }

    companion object {
        fun inject(target: PostsDogFragment){
            val component = DaggerFeaturePostsDogComponent.builder()
                .bind(target)
                .coreDataComponent(CoreDataComponent.build())
                .build()
            component.inject(target)
        }
    }
}

@Module
class FeaturePostsDogModule {
    @FeaturePostsDogScope
    @Provides
    @[IntoMap ViewModelKey(PostsDogViewModel::class)]
    fun provideViewModel(postsInteractor: PostsInteractor): ViewModel =
        PostsDogViewModel(postsInteractor)

    @Provides
    fun providePostsAdapter(): PostsRecyclerAdapter =
        PostsRecyclerAdapter()

    @Provides
    fun provideStaggeredManager(): StaggeredGridLayoutManager =
        StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)
}