package com.rush.feature_posts_top.presentation.adapter

import com.rush.core_presentation.base.BaseViewHolder
import com.rush.core_presentation.model.PostItemModel
import com.rush.feature_posts_top.databinding.ItemPostBinding

class PostViewHolder(private val itemBinding: ItemPostBinding) : BaseViewHolder<PostItemModel>(itemBinding.root) {

    override fun bind(item: PostItemModel) {
        itemBinding.plivPost.bind(item)
    }

}