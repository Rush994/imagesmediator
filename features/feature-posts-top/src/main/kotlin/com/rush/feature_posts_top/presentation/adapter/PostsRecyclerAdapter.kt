package com.rush.feature_posts_top.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.rush.core_presentation.model.PostItemModel
import com.rush.core_presentation.tool.PostsDiffUtil
import com.rush.feature_posts_top.databinding.ItemPostBinding

class PostsRecyclerAdapter: ListAdapter<PostItemModel, PostViewHolder>(PostsDiffUtil()) {

    private lateinit var binding: ItemPostBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder{
        binding = ItemPostBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PostViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    override fun getItemCount(): Int = currentList.count()
}