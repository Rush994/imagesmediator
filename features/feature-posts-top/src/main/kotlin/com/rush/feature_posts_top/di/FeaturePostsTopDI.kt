package com.rush.feature_posts_top.di

import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.rush.core_data.di.CoreDataComponent
import com.rush.core_domain.interactor.PostsInteractor
import com.rush.core_presentation.di.Injector
import com.rush.core_presentation.di.ViewModelKey
import com.rush.feature_posts_top.ui.PostsTopFragment
import com.rush.feature_posts_top.presentation.PostsTopViewModel
import com.rush.feature_posts_top.presentation.adapter.PostsRecyclerAdapter
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Scope

@Scope
annotation class FeaturePostsTopScope

@FeaturePostsTopScope
@Component(
    modules = [FeaturePostsTopModule::class],
    dependencies = [CoreDataComponent::class]
)
interface FeaturePostsTopComponent : Injector<PostsTopFragment> {

    @Component.Builder
    interface Builder{
        @BindsInstance
        fun bind(target: PostsTopFragment): Builder
        fun coreDataComponent(component: CoreDataComponent): Builder
        fun build(): FeaturePostsTopComponent
    }

    companion object {
        fun inject(fragment: PostsTopFragment){
            val component = DaggerFeaturePostsTopComponent.builder()
                .bind(fragment)
                .coreDataComponent(CoreDataComponent.build())
                .build()
            component.inject(fragment)
        }
    }
}

@Module
class FeaturePostsTopModule {
    @Provides
    @[IntoMap ViewModelKey(PostsTopViewModel::class)]
    fun provideViewModel(postsInteractor: PostsInteractor): ViewModel =
        PostsTopViewModel(postsInteractor)

    @Provides
    fun providePostsAdapter(): PostsRecyclerAdapter =
        PostsRecyclerAdapter()

    @Provides
    fun provideStaggeredManager(): StaggeredGridLayoutManager =
        StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)
}