package com.rush.container_main.presentation

import com.rush.container_main.navigation.router.MainContainerRouter
import com.rush.core_presentation.base.BaseViewModel
import javax.inject.Inject

class MainContainerViewModel @Inject constructor(
    private val router: MainContainerRouter
): BaseViewModel() {

    fun navigateOnStart() =
        router.newRootToMainFlowScreen()

}