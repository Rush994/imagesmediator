package com.rush.container_main.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.rush.container_main.R
import com.rush.container_main.di.MainContainerComponent
import com.rush.container_main.navigation.LifecycleAwareNavigatorHolder
import com.rush.container_main.presentation.MainContainerViewModel
import com.rush.core_presentation.base.BaseActivity
import javax.inject.Inject

class MainContainerActivity : BaseActivity(R.layout.activity_container_main) {

    @Inject
    lateinit var navigatorHolder: LifecycleAwareNavigatorHolder

    @Inject
    lateinit var viewModel: MainContainerViewModel

    override fun inject() {
        MainContainerComponent.inject(this)
    }

    override fun initViews() {
        navigatorHolder.register(this)
        viewModel.navigateOnStart()
    }
}