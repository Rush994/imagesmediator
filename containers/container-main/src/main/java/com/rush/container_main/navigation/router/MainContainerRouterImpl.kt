package com.rush.container_main.navigation.router

import com.github.terrakok.cicerone.Router
import com.rush.feature_main.navigation.MainFlowScreenNavigation
import javax.inject.Inject

class MainContainerRouterImpl @Inject constructor(
    private val appRouter: Router
) : MainContainerRouter {
    override fun newRootToMainFlowScreen() {
        appRouter.newRootScreen(MainFlowScreenNavigation.getFragment())
    }
}