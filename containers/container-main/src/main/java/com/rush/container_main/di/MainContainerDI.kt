package com.rush.container_main.di

import androidx.lifecycle.ViewModel
import com.rush.container_main.navigation.router.MainContainerRouter
import com.rush.container_main.ui.MainContainerActivity
import com.rush.container_main.presentation.MainContainerViewModel
import com.rush.core_presentation.di.Injector
import com.rush.core_presentation.di.MainContainerScope
import com.rush.core_presentation.di.ViewModelKey
import dagger.*
import dagger.multibindings.IntoMap
import com.rush.core_navigation.di.CoreNavigationComponent

@MainContainerScope
@Component(
    modules = [MainContainerModule::class,
        MainContainerRouterModule::class],
    dependencies = [CoreNavigationComponent::class]
)
interface MainContainerComponent : Injector<MainContainerActivity> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(target: MainContainerActivity): Builder
        fun coreNavigation(target: CoreNavigationComponent): Builder
        fun build(): MainContainerComponent
    }

    companion object {
        fun inject(target: MainContainerActivity) {
            val component = DaggerMainContainerComponent.builder()
                .bind(target)
                .coreNavigation(CoreNavigationComponent.build())
                .build()
            component.inject(target)
        }
    }
}

@Module
class MainContainerModule {
    @MainContainerScope
    @Provides
    @IntoMap
    @ViewModelKey(MainContainerViewModel::class)
    fun provideViewModel(router: MainContainerRouter): ViewModel =
        MainContainerViewModel(router)
}