package com.rush.container_main.di

import androidx.lifecycle.LifecycleObserver
import com.github.terrakok.cicerone.Command
import com.github.terrakok.cicerone.Navigator
import com.github.terrakok.cicerone.androidx.AppNavigator
import com.rush.container_main.R
import com.rush.container_main.navigation.LifecycleAwareNavigatorHolder
import com.rush.container_main.navigation.router.MainContainerRouter
import com.rush.container_main.navigation.router.MainContainerRouterImpl
import com.rush.container_main.ui.MainContainerActivity
import com.rush.core_presentation.di.MainContainerScope
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
interface MainContainerRouterModule {

    @MainContainerScope
    @Binds
    fun bindMainContainerRouter(impl: MainContainerRouterImpl): MainContainerRouter

    @MainContainerScope
    @Binds
    fun bindLifecycleObserver(impl: LifecycleAwareNavigatorHolder): LifecycleObserver

    companion object {

        @MainContainerScope
        @Provides
        fun provideNavigator(activity: MainContainerActivity): Navigator =
            object : AppNavigator(activity, R.id.mainContainer) {
                override fun applyCommands(commands: Array<out Command>) {
                    super.applyCommands(commands)
                    activity.supportFragmentManager.executePendingTransactions()
                }
            }
    }
}