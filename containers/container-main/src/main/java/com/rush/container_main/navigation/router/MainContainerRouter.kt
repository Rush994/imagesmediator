package com.rush.container_main.navigation.router

interface MainContainerRouter {
    fun newRootToMainFlowScreen()
}