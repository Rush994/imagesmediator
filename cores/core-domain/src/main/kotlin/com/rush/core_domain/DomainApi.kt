package com.rush.core_domain

import com.rush.core_domain.interactor.PostsInteractor
import com.rush.core_domain.usecase.*

interface DomainApi {
    fun fetchPostsListUseCase(): FetchPostsListUseCase
    fun fetchPostUseCase(): FetchPostUseCase
    fun observeImagesUseCase(): ObserveImagesUseCase
    fun observeJointPostUseCase(): ObserveJointPostUseCase
    fun observePostListTopUseCase(): ObservePostListTopUseCase

    fun postsInteractor(): PostsInteractor
}