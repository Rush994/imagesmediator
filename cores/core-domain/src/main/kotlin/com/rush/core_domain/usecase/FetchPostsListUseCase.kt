package com.rush.core_domain.usecase

import com.rush.core_domain.repository.PostsRepository
import io.reactivex.rxjava3.core.Completable
import javax.inject.Inject

class FetchPostsListUseCase @Inject constructor(
    private val postsRepository: PostsRepository
) {
    operator fun invoke(name: String, page: Int, postType: Int): Completable =
        postsRepository.fetchPosts(name, page, postType)
}