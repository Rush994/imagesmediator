package com.rush.core_domain.entity

data class CommentEntity(
    val id: Long,
    val imageId: String,
    val comment: String,
    val author: String,
    val ups: Long,
    val downs: Long = 0,
    val platform: String
)
