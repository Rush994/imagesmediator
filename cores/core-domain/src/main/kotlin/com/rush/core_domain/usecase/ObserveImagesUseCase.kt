package com.rush.core_domain.usecase

import com.rush.core_domain.entity.ImageEntity
import com.rush.core_domain.repository.PostsRepository
import io.reactivex.rxjava3.core.Flowable
import javax.inject.Inject

class ObserveImagesUseCase @Inject constructor(
    private val postsRepository: PostsRepository
){
    operator fun invoke(postId: String): Flowable<List<ImageEntity>> =
        postsRepository.observeImages(postId)
}