package com.rush.core_domain.repository

import android.content.Context
import com.rush.core_domain.entity.ImageEntity
import com.rush.core_domain.entity.JointPostEntity
import com.rush.core_domain.entity.PostEntity
import com.rush.core_domain.entity.TagEntity
import com.rush.core_presentation.type.CategoryTypes
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single

interface PostsRepository {

    fun fetchPost(postId: String, categoryType: Int): Completable

    fun fetchPosts(name:String, page: Int, postType: Int): Completable

    fun findPost(postId: String): Flowable<PostEntity>

    fun observePosts(postType: Int): Flowable<List<PostEntity>>

    fun observeTags(postId: String): Flowable<List<TagEntity>>

    fun observeImages(postId: String): Flowable<List<ImageEntity>>

    fun observeJointPost(postId: String): Flowable<JointPostEntity>

    fun observeJointPostDeepLink(postId: String, categoryType: CategoryTypes): Flowable<JointPostEntity>

    fun insertImage(post: PostEntity): Completable

    fun loadImages(postId: String, postType: Int): Completable

    fun loadComments(postId: String): Completable

    fun deletePostsByTag(): Completable

    fun fetchTags(): Single<List<TagEntity>>

    fun mediaDownload(folderPath:String, filePath: String, mediaUri: String, context: Context): Completable

}