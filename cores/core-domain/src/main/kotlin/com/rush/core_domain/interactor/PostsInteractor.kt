package com.rush.core_domain.interactor

import com.rush.core_domain.usecase.*
import com.rush.core_presentation.type.CategoryTypes
import javax.inject.Inject

class PostsInteractor @Inject constructor(
    private val fetchPostUseCase: FetchPostUseCase,
    private val fetchPostsListUseCase: FetchPostsListUseCase,

    private val observeImagesUseCase: ObserveImagesUseCase,
    private val observePostListTopUseCase: ObservePostListTopUseCase,
    private val observeJointPostUseCase: ObserveJointPostUseCase
) {
    fun fetchPost(postId: String) =
        fetchPostUseCase(postId, CategoryTypes.Single.value)

    fun fetchPosts(name: String = "", page: Int, postType: Int) =
        fetchPostsListUseCase(name, page, postType)

    fun observeImages(postId: String) =
        observeImagesUseCase(postId)

    fun observePosts(postType: Int) =
        observePostListTopUseCase(postType)

    fun observeJointPost(postId: String, categoryType: CategoryTypes) =
        observeJointPostUseCase(postId, categoryType)
}