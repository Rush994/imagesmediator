package com.rush.core_domain.usecase

import com.rush.core_domain.entity.PostEntity
import com.rush.core_domain.repository.PostsRepository
import io.reactivex.rxjava3.core.Flowable
import javax.inject.Inject

class ObservePostListTopUseCase @Inject constructor(
    private val postsRepository: PostsRepository
){
    operator fun invoke(postType: Int): Flowable<List<PostEntity>> =
        postsRepository.observePosts(postType)
}