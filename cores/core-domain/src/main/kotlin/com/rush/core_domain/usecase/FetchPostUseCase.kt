package com.rush.core_domain.usecase

import com.rush.core_domain.repository.PostsRepository
import javax.inject.Inject

class FetchPostUseCase @Inject constructor(
    private val postsRepository: PostsRepository
) {
    operator fun invoke(postId:String, categoryTypes: Int) =
        postsRepository.fetchPost(postId, categoryTypes)
}