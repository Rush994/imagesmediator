package com.rush.core_domain.entity

import com.rush.core_presentation.model.PostItemModel
import com.rush.core_presentation.type.CategoryTypes

data class PostEntity(
    val id: String = "",
    val mediaId: String = "",
    val title: String = "",
    val datetime: Long = 0,
    val cover: String? = null,
    val owner: String = "",
    val views: Int = 0,
    val link: String = "",
    val thumbUps: Int = 0,
    val thumbDowns: Int = 0,
    val commentCount: Int = 0,
    val isAlbum: Boolean = false,
    var mediaCount: Int? = null,
    val categoryType: CategoryTypes = CategoryTypes.Default,
    val images: List<ImageEntity> = listOf()
)

fun PostEntity.toPresentation() =
    PostItemModel(
        id = this.id,
        mediaId = this.mediaId,
        title = this.title,
        datetime = this.datetime,
        cover = this.cover,
        owner = this.owner,
        views = this.views,
        link = this.link,
        thumbUps = this.thumbUps,
        thumbDowns = this.thumbDowns,
        commentCount = this.commentCount,
        isAlbum = this.isAlbum,
        mediaCount = this.mediaCount,
        categoryType = this.categoryType
    )

fun List<PostEntity>.toPresentation() =
    this.map { it.toPresentation() }

