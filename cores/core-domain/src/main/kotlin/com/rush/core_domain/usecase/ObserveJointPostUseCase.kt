package com.rush.core_domain.usecase

import com.rush.core_domain.entity.JointPostEntity
import com.rush.core_domain.repository.PostsRepository
import com.rush.core_presentation.type.CategoryTypes
import io.reactivex.rxjava3.core.Flowable
import javax.inject.Inject

class ObserveJointPostUseCase @Inject constructor(
    private val postsRepository: PostsRepository
) {
    operator fun invoke(postId: String, categoryType: CategoryTypes): Flowable<JointPostEntity> =
        postsRepository.observeJointPostDeepLink(postId, categoryType)
}