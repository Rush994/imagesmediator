package com.rush.core_presentation.base

import android.content.Context

interface LocalizedContextProvider {
    fun provideContext(): Context?
}