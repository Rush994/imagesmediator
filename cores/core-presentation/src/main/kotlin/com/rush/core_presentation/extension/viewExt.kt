package com.rush.core_presentation.extension

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.graphics.Typeface
import android.util.Log
import android.view.View
import android.view.ViewAnimationUtils
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import timber.log.Timber

fun Context.toast(message: String) =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

fun Context.logging(message: String, tag: String) =
    Timber.d(message)

fun Context.colorChanger(@ColorRes id: Int) =
    ContextCompat.getColor(this, id)

fun Snackbar.changeFont() {
    val tv = view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
    val font = Typeface.createFromAsset(context.assets, "segoeui_light.ttf")
    tv.typeface = font
}

//region Animation handling
fun Context.revealEditText(view: ConstraintLayout) {
    val cx = view.right - 30
    val cy = view.bottom - 60
    val finalRadius = Math.max(view.width, view.height)
    val anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0f, finalRadius.toFloat())
    view.visibility = View.VISIBLE
    anim.start()
}

fun Context.hideEditText(view: ConstraintLayout) {
    val cx = view.right - 30
    val cy = view.bottom - 60
    val initialRadius = view.width
    val anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, initialRadius.toFloat(), 0f)
    anim.addListener(object : AnimatorListenerAdapter() {
        override fun onAnimationEnd(animation: Animator) {
            super.onAnimationEnd(animation)
            view.visibility = View.INVISIBLE
        }
    })
    anim.start()
}
//endregion

