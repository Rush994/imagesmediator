package com.rush.core_presentation.model

sealed class PostContents {
    data class ImageItem(val model: ImageModel): PostContents()
}