package com.rush.core_presentation.provider

import android.content.Context
import androidx.annotation.StringRes
import javax.inject.Inject

class ResourceProviderImpl @Inject constructor(
    context: Context
) : ResourceProvider {

    val appContext: Context = context.applicationContext

    private fun getContext(): Context = appContext

    override fun getString(@StringRes resId: Int): String = getContext().getString(resId)

    override fun getString(@StringRes resId: Int, vararg formatArgs: Any): String =
        getContext().getString(resId, *formatArgs)


    override fun getText(resId: Int): CharSequence =
        getContext().getText(resId)
}