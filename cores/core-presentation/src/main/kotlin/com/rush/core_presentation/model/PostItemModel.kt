package com.rush.core_presentation.model

import kotlinx.parcelize.Parcelize
import android.os.Parcelable
import com.rush.core_presentation.type.CategoryTypes

@Parcelize
data class PostItemModel(
    var id: String = "",
    var mediaId: String = "",
    val title: String = "",
    val datetime: Long = 0,
    val cover: String? = null,
    val owner: String = "",
    val views: Int = 0,
    val link: String = "",
    val thumbUps: Int = 0,
    val thumbDowns: Int = 0,
    val commentCount: Int = 0,
    val isAlbum: Boolean = false,
    var mediaCount: Int? = null,
    val categoryType: CategoryTypes = CategoryTypes.ByTag
) : Parcelable