package com.rush.core_presentation.base

import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.kotlin.addTo

open class BaseViewModel: ViewModel() {
    private val aDisposables: CompositeDisposable by lazy { CompositeDisposable() }
    private val mDisposables: CompositeDisposable by lazy { CompositeDisposable() }

    override fun onCleared() {
        super.onCleared()
        aDisposables.clear()
    }

    protected fun Disposable.autoDispose() = addTo(aDisposables)
    protected fun Disposable.manualDispose() = addTo(mDisposables)

    open fun back() {
        mDisposables.clear()
    }
}