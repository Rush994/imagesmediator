package com.rush.core_presentation.base

data class BaseState(
    var targetVisible: Boolean = false,
    var completed: Boolean = false,
    var errorText: String? = null
)