package com.rush.core_presentation.extension

import androidx.lifecycle.Lifecycle
import com.rush.core_presentation.rx.AutoDisposable
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.processors.PublishProcessor
import io.reactivex.rxjava3.schedulers.Schedulers

fun Completable.performOnIO(): Completable {
    return subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Single<T>.performOnIO(): Single<T> {
    return subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Observable<T>.performOnIO(): Observable<T> {
    return subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Flowable<T>.performOnIO(): Flowable<T> {
    return subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}

fun <T> PublishProcessor<T>.performOnIO(): Flowable<T> {
    return subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}

fun Disposable.disposeOnDestroy(autoDisposable: AutoDisposable) {
    autoDisposable.add(Lifecycle.Event.ON_DESTROY, this)
}

fun Disposable.disposeOnPause(autoDisposable: AutoDisposable) {
    autoDisposable.add(Lifecycle.Event.ON_PAUSE, this)
}

fun Disposable.disposeOnStop(autoDisposable: AutoDisposable) {
    autoDisposable.add(Lifecycle.Event.ON_STOP, this)
}

fun Disposable.disposeOnAny(autoDisposable: AutoDisposable) {
    autoDisposable.add(Lifecycle.Event.ON_ANY, this)
}