package com.rush.core_presentation.type

enum class TagTypes(val value: String){
    Cat("cats"),
    Dog("dogs_are_the_best_people")
}