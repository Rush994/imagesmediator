package com.rush.core_presentation.action

import com.rush.core_presentation.type.CategoryTypes

sealed class PostsListActions{
    data class FirstFetchList(val type: CategoryTypes = CategoryTypes.Default): PostsListActions()
    data class FetchNextPortion(val isRefresh: Boolean = false): PostsListActions()
    data class DisplayFetchedPortion(val totalItemCount: Int, val lastVisibleItem: Int): PostsListActions()
}
