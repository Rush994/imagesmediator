package com.rush.core_presentation.model

data class VideoModel(
    val postId: String,
    val mediaId: String,
    val title: String?,
    val description: String?,
    val datetime: Long,
    val mp4: String
)
