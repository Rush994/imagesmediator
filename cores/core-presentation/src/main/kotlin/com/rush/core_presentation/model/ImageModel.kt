package com.rush.core_presentation.model

data class ImageModel(
    val postId: String,
    val mediaId: String,
    val title: String?,
    val description: String?,
    val datetime: Long,
    var link: String
)