package com.rush.core_presentation.provider

import androidx.annotation.StringRes


interface ResourceProvider {
    fun getString(@StringRes resId: Int): String
    fun getString(@StringRes resId: Int, vararg formatArgs: Any): String
    fun getText(@StringRes resId: Int): CharSequence
}