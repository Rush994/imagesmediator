package com.rush.core_presentation.di

interface Injector<in T> {
    fun inject(target: T)
}