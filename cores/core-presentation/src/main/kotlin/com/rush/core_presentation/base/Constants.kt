package com.rush.core_presentation.base

object Constants {
    const val POST_ID = "PostId"
    const val POST_DATA = "PostData"
    const val ALPHA_NO_CHECKED_TAG = 54
    const val ALPHA_CHEKED_TAG = 225
    const val LOADING_POSTS = "LoadingPosts"
    const val MEDIATOR_COMMON_TAG = "MEDIATOR_COMMON_TAG"
    const val MEDIA_URI = "MediaUri"
    const val NO_COMMENTS = "NoComments"
    const val WRITE_REQUEST_CODE = 300
    const val FINE_REQUEST_CODE = 400
    const val PERMISSION_TAG = "Permission"
    const val DOWNLOAD_NOTIFICATION_ID = 1
    const val COMPLETE_NOTIFICATION_ID = 2
    const val DOWLOADING = "DownloadingFile"
    const val COMPLETED = "DownloadCompleted"
    const val RECYCLER_EXO_TAG = "RecyclerExoplayer"
    const val THRESHOLD_RECYCLER_LOADER = 3
    const val THRESHOLD_RECYCLER_VISIBLE_ITEMS = 10
}