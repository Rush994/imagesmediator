package com.rush.core_presentation.tool

import androidx.recyclerview.widget.DiffUtil
import com.rush.core_presentation.model.PostItemModel

class PostsDiffUtil: DiffUtil.ItemCallback<PostItemModel>() {
    override fun areItemsTheSame(
        oldItem: PostItemModel,
        newItem: PostItemModel
    ): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(
        oldItem: PostItemModel,
        newItem: PostItemModel
    ): Boolean =
        oldItem == newItem
}