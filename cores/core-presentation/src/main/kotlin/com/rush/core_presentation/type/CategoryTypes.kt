package com.rush.core_presentation.type

enum class CategoryTypes(val value: Int){
    Default(0),
    ByTag(1),
    Top(2),
    Cat(3),
    Dog(4),
    Single(5)
}