package com.rush.core_presentation.model

data class TagModel(
    val name: String,
    val displayName: String,
    val followers: Int,
    val totalItems: Int,
    var isChecked: Boolean = false
)