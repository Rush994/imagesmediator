package com.rush.core_presentation.extension

import android.content.res.Resources
import kotlin.math.roundToInt

fun Float.spToPx(): Float = (this * Resources.getSystem().displayMetrics.scaledDensity)

fun Float.dpToPx(): Float = (this * Resources.getSystem().displayMetrics.density)

fun Int.dpToPx(): Int = ((this * Resources.getSystem().displayMetrics.density).roundToInt())

fun Float.pxToDp(): Float = (this / Resources.getSystem().displayMetrics.density)

fun Int.pxToDp(): Int = ((this / Resources.getSystem().displayMetrics.density).roundToInt())