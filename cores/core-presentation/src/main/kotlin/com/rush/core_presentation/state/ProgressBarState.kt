package com.rush.core_presentation.state

data class ProgressBarState(
    val isProgress: Boolean = true
)
