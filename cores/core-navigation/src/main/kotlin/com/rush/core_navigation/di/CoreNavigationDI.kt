package com.rush.core_navigation.di

import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.Router
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class CoreNavigationScope

@CoreNavigationScope
@Component(modules = [
    CoreNavigationModule::class
])
interface CoreNavigationComponent{

    fun router(): Router
    fun navigatorHolder(): NavigatorHolder

    companion object{
        fun build(): CoreNavigationComponent =
            DaggerCoreNavigationComponent.builder().build()
    }
}

@Module
class CoreNavigationModule{
    @CoreNavigationScope
    @Provides
    fun provideCicerone(): Cicerone<Router> = Cicerone.create()

    @CoreNavigationScope
    @Provides
    fun provideRouter(cicerone: Cicerone<Router>): Router = cicerone.router

    @CoreNavigationScope
    @Provides
    fun provideNavigationHolder(cicerone: Cicerone<Router>): NavigatorHolder = cicerone.getNavigatorHolder()
}