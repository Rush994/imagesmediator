package com.rush.core_navigation

import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.Router
import com.rush.core_presentation.di.MainContainerScope
import dagger.Module
import dagger.Provides

@Module
class NavigationModule {
    @MainContainerScope
    @Provides
    fun provideCicerone() = Cicerone.create()

    @MainContainerScope
    @Provides
    fun provideRouter(cicerone: Cicerone<Router>) = cicerone.router

    @MainContainerScope
    @Provides
    fun provideNavigationHolder(cicerone: Cicerone<Router>) = cicerone.getNavigatorHolder()
}