package com.rush.core_data.network.source

import com.rush.core_data.network.api.ImgurApi
import com.rush.core_data.network.api.response.*
import com.rush.core_data.network.response.PostRespModel
import com.rush.core_presentation.type.TagTypes
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class PostsRemoteSourceImpl @Inject constructor(
    private val api: ImgurApi
) : PostsRemoteSource {
    override fun getPostListTop(page: Int): Single<List<PostRespModel>> =
        api.getTopPostList(page).map {
            it.data
        }

    override fun getPostListCat(page: Int): Single<List<PostRespModel>> =
        api.getPostListByTag(TagTypes.Cat.value, page).map {
            it.data.items
        }

    override fun getPostListDog(page: Int): Single<List<PostRespModel>> =
        api.getPostListByTag(TagTypes.Dog.value, page).map {
            it.data.items
        }

    override fun getPostListByTag(name: String, page: Int): Single<List<PostRespModel>> =
        api.getPostListByTag(name, page).map {
            it.data.items
        }

    override fun getPostInner(postId: String): Single<PostSingleRespModel> {
        TODO("Not yet implemented")
    }

    override fun getCommentList(postId: String): Single<CommentListRespModel> {
        TODO("Not yet implemented")
    }

    override fun getTagList(): Single<TagListDataRespModel> {
        TODO("Not yet implemented")
    }

}