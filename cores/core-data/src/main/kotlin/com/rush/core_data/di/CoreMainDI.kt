package com.rush.core_data.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.rush.core_data.Core
import com.rush.core_data.di.module.NetworkModule
import com.rush.core_data.di.module.DataModule
import com.rush.core_data.di.module.RoomModule
import com.rush.core_data.di.module.ViewModelFactoryModule
import com.rush.core_domain.DomainApi
import com.rush.core_domain.repository.PostsRepository
import dagger.*
import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class CoreMainScope

@CoreMainScope
@Component(
    modules = [CoreModule::class,
        DataModule::class,
        RoomModule::class,
        NetworkModule::class,
        ViewModelFactoryModule::class]
)
interface CoreDataComponent: DomainApi {

    fun postsRepository(): PostsRepository

    companion object {
        fun build(): CoreDataComponent =
            DaggerCoreDataComponent.builder()
                .build()
    }
}

@Module
class CoreModule {

    companion object {
        const val COMMON_SHARED_PREFS = "COMMON_SHARED_PREFS"
    }

    @CoreMainScope
    @Provides
    fun provideAppContext(): Application {
        return Core.getApplication()
    }

    @CoreMainScope
    @Provides
    fun provideCommonSharedPreferences(application: Application): SharedPreferences {
        return application.getSharedPreferences(COMMON_SHARED_PREFS, Context.MODE_PRIVATE)
    }
}