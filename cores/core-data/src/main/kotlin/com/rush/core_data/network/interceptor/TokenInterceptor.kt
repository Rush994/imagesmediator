package com.rush.core_data.network.interceptor

import com.rush.core_data.network.api.ImgurApi
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class TokenInterceptor @Inject constructor(): Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = request.newBuilder()
            .addHeader("Authorization", "Client-ID ${ImgurApi.API_KEY}")
            .build()

        return chain.proceed(request)
    }
}