package com.rush.core_data.room.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import com.rush.core_data.room.AppDatabase

@Entity(
    tableName = AppDatabase.TABLE_POST_TAG_JOINS,
    primaryKeys = (arrayOf("postId", "tagId")),
    foreignKeys = [
        ForeignKey(
            entity = PostDBModel::class,
            parentColumns = arrayOf("Id"),
            childColumns = arrayOf("postId"),
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = TagDBModel::class,
            parentColumns = arrayOf("Id"),
            childColumns = arrayOf("tagId"),
            onDelete = ForeignKey.CASCADE
        )])
data class PostTagJoinDBModel(
    var postId: String,
    @ColumnInfo(index = true)
    var tagId: Long
)