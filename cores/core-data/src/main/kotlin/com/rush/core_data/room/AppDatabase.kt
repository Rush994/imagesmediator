package com.rush.core_data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.rush.core_data.room.dao.*
import com.rush.core_data.room.model.*

@Database(entities = [
    PostDBModel::class,
    ImageDBModel::class,
    CommentDBModel::class,
    TagDBModel::class,
    PostTagJoinDBModel::class],
    version = 1,
    exportSchema = false)
abstract class AppDatabase: RoomDatabase() {
    companion object{
        const val DATABASE_NAME = "ImgurMediatorDatabase"

        const val TABLE_POSTS = "Posts"
        const val TABLE_IMAGES = "Images"
        const val TABLE_COMMENTS = "Comments"
        const val TABLE_POST_TAG_JOINS = "PostTagJoins"

        const val TABLE_POST_TAGS = "PostTags"
    }

    abstract fun getPostsOutersDao(): PostsDao
    abstract fun getPostInnersDao(): ImagesDao
    abstract fun getCommentsDao(): CommentsDao
    abstract fun getPostTagsDao(): TagsDao
    abstract fun getPostTagJoinsDao(): PostTagJoinsDao
}