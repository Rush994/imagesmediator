package com.rush.core_data.network.api.response

import com.rush.core_data.network.response.CommentRespModel
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CommentListRespModel(
    @Json(name = "data")
    val data: List<CommentRespModel>,
    @Json(name = "success")
    val success: Boolean,
    @Json(name = "status")
    val status: Int
)