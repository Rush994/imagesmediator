package com.rush.core_data.room.dao

import androidx.room.Dao
import com.rush.core_data.room.AppDatabase
import com.rush.core_data.room.model.TagDBModel

@Dao
abstract class TagsDao: BaseDao<TagDBModel>(AppDatabase.TABLE_POST_TAGS) {

}