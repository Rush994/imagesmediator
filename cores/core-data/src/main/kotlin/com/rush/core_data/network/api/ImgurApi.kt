package com.rush.core_data.network.api

import com.rush.core_data.network.api.response.CommentListRespModel
import com.rush.core_data.network.api.response.PostListByTagRespModel
import com.rush.core_data.network.api.response.PostListRespModel
import com.rush.core_data.network.api.response.TagListDataRespModel
import com.rush.core_data.network.response.PostRespModel
import com.rush.core_domain.BuildConfig
import io.reactivex.rxjava3.core.Single
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface ImgurApi {
    companion object {
        const val BASE_URL = "https://api.imgur.com/"
        const val API_KEY = "046823c07ef6628"

        fun getApiInstance(tokenInterceptor: Interceptor): Retrofit {
            val httpClient = OkHttpClient.Builder().also {
                it.addInterceptor(HttpLoggingInterceptor().apply {
                    level = if (BuildConfig.DEBUG)
                        HttpLoggingInterceptor.Level.BODY
                    else
                        HttpLoggingInterceptor.Level.NONE
                }).addInterceptor(tokenInterceptor)
            }
            return Retrofit.Builder()
                .client(httpClient.build())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
        }
    }

    @GET("3/gallery/top/{page}?showViral=false")
    fun getTopPostList(@Path("page") page: Int): Single<PostListRespModel>

    @GET("3/gallery/t/{tag}/{page}")
    fun getPostListByTag(
        @Path("tag") tag: String,
        @Path("page") page: Int
    ): Single<PostListByTagRespModel>

    @GET("3/gallery/album/{galleryHash}")
    fun getPostInfoInner(@Path("galleryHash") postId: String): Single<PostRespModel>

    @GET("3/image/{imageHash}")
    fun getImageInfo(@Path("imageHash") imageId: String): Single<PostRespModel>

    @GET("3/gallery/{imageHash}/comments/best")
    fun getPostCommentList(@Path("imageHash") postId: String): Single<CommentListRespModel>

    @GET("3/tags")
    fun getTagList(): Single<TagListDataRespModel>
}