package com.rush.core_data.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.rush.core_data.room.AppDatabase
import com.rush.core_data.room.model.PostTagJoinDBModel
import com.rush.core_data.room.model.TagDBModel
import io.reactivex.rxjava3.core.Flowable

@Dao
abstract class PostTagJoinsDao: BaseDao<PostTagJoinDBModel>(AppDatabase.TABLE_POST_TAG_JOINS){
    @Query("""SELECT * FROM PostTags INNER JOIN PostTagJoins ON
                    PostTags.Id=PostTagJoins.tagId WHERE
                    PostTagJoins.postId LIKE :postId""")
    abstract fun observePostTags(postId: String): Flowable<List<TagDBModel>>
}