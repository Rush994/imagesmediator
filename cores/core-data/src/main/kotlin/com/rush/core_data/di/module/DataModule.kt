package com.rush.core_data.di.module

import com.rush.core_data.di.CoreMainScope
import com.rush.core_data.network.source.PostsRemoteSource
import com.rush.core_data.network.source.PostsRemoteSourceImpl
import com.rush.core_data.repository.PostsRepositoryImpl
import com.rush.core_data.room.source.PostsLocalSource
import com.rush.core_data.room.source.PostsLocalSourceImpl
import com.rush.core_domain.repository.PostsRepository
import dagger.Binds
import dagger.Module

@Module
abstract class DataModule {

    @CoreMainScope
    @Binds
    abstract fun bindPostsLocalSource(impl: PostsLocalSourceImpl): PostsLocalSource

    @CoreMainScope
    @Binds
    abstract fun bindPostsRemoteSource(impl: PostsRemoteSourceImpl): PostsRemoteSource

    @CoreMainScope
    @Binds
    abstract fun bindPostsRepository(impl: PostsRepositoryImpl) : PostsRepository
}