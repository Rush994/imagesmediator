package com.rush.core_data.network.api.response

import com.rush.core_data.network.response.PostRespModel
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PostSingleRespModel(
    @Json(name = "data")
    val data: PostRespModel,
    @Json(name = "success")
    val success: Boolean,
    @Json(name ="status")
    val status: Int
)
