package com.rush.core_data.network.source

import com.rush.core_data.network.api.response.*
import com.rush.core_data.network.response.PostRespModel
import io.reactivex.rxjava3.core.Single

interface PostsRemoteSource {
    fun getPostListTop(page: Int): Single<List<PostRespModel>>
    fun getPostListCat(page: Int): Single<List<PostRespModel>>
    fun getPostListDog(page: Int): Single<List<PostRespModel>>
    fun getPostListByTag(name: String, page: Int): Single<List<PostRespModel>>

    fun getPostInner(postId: String): Single<PostSingleRespModel>

    fun getCommentList(postId: String): Single<CommentListRespModel>

    fun getTagList(): Single<TagListDataRespModel>
}