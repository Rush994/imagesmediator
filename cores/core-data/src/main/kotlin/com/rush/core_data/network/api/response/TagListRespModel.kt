package com.rush.core_data.network.api.response

import com.rush.core_data.network.response.TagRespModel
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TagListRespModel(
    @Json(name = "tags")
    val tags: List<TagRespModel>,
    @Json(name = "featured")
    val featured: String
)
