package com.rush.core_data.repository

import android.content.Context
import com.rush.core_data.network.response.PostRespModel
import com.rush.core_data.network.response.toDB
import com.rush.core_data.network.source.PostsRemoteSource
import com.rush.core_data.room.model.toEntity
import com.rush.core_data.room.source.PostsLocalSource
import com.rush.core_domain.entity.ImageEntity
import com.rush.core_domain.entity.JointPostEntity
import com.rush.core_domain.entity.PostEntity
import com.rush.core_domain.entity.TagEntity
import com.rush.core_domain.repository.PostsRepository
import com.rush.core_presentation.type.CategoryTypes
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import timber.log.Timber
import javax.inject.Inject

class PostsRepositoryImpl @Inject constructor(
    private val postsRemoteSource: PostsRemoteSource,
    private val postsLocalSource: PostsLocalSource
): PostsRepository {
    override fun fetchPost(postId: String, categoryType: Int): Completable =
        TODO()

    override fun fetchPosts(name: String, page: Int, postType: Int): Completable =
        loadPostsDependsOnType(name, page, postType)
            .map { it.toDB(postType) }
            .flatMap {
                if (page == 1)
                    postsLocalSource.deletePosts(postType).toSingle { it }
                else
                    Single.just(it)
            }
            .flatMapCompletable {
                postsLocalSource.insertPosts(it)
            }.doOnError {
                Timber.e(it)
            }

    override fun findPost(postId: String): Flowable<PostEntity> {
        TODO("Not yet implemented")
    }

    override fun observePosts(postType: Int): Flowable<List<PostEntity>> =
        postsLocalSource.observePostsByType(postType)
            .map {
                it.toEntity()
            }

    override fun observeTags(postId: String): Flowable<List<TagEntity>> {
        TODO("Not yet implemented")
    }

    override fun observeImages(postId: String): Flowable<List<ImageEntity>> {
        TODO("Not yet implemented")
    }

    override fun observeJointPost(postId: String): Flowable<JointPostEntity> {
        TODO("Not yet implemented")
    }

    override fun observeJointPostDeepLink(
        postId: String,
        categoryType: CategoryTypes
    ): Flowable<JointPostEntity> {
        TODO("Not yet implemented")
    }

    override fun insertImage(post: PostEntity): Completable {
        TODO("Not yet implemented")
    }

    override fun loadImages(postId: String, postType: Int): Completable {
        TODO("Not yet implemented")
    }

    override fun loadComments(postId: String): Completable {
        TODO("Not yet implemented")
    }

    override fun deletePostsByTag(): Completable {
        TODO("Not yet implemented")
    }

    override fun fetchTags(): Single<List<TagEntity>> {
        TODO("Not yet implemented")
    }

    override fun mediaDownload(
        folderPath: String,
        filePath: String,
        mediaUri: String,
        context: Context
    ): Completable {
        TODO("Not yet implemented")
    }

    private fun loadPostsDependsOnType(
        name: String,
        page: Int,
        postType: Int
    ): Single<List<PostRespModel>> =
        when (postType) {
            CategoryTypes.Top.value -> postsRemoteSource.getPostListTop(page)
            CategoryTypes.Cat.value -> postsRemoteSource.getPostListCat(page)
            CategoryTypes.Dog.value -> postsRemoteSource.getPostListDog(page)
            CategoryTypes.ByTag.value -> postsRemoteSource.getPostListByTag(name, page)
            else -> {
                Single.error(Throwable("Not valid posts"))
            }
        }

}