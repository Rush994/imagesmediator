package com.rush.core_data

import android.app.Application
import com.rush.core_data.di.CoreDataComponent
import timber.log.Timber

object Core {

    private var application: Application? = null

    fun initialize(application: Application){
        Timber.i("[Core] ImgurAlbums Core 1.00 is initialized")
        this.application = application
        CoreDataComponent.build()
    }

    fun getApplication(): Application {
        return application ?: Application()
    }
}