package com.rush.core_data.room.model

import androidx.room.*
import com.rush.core_data.room.AppDatabase

@Entity(tableName = AppDatabase.TABLE_IMAGES,
    foreignKeys = [
        ForeignKey(
            entity = PostDBModel::class,
            parentColumns = ["Id"],
            childColumns = ["PostId"],
            onDelete = ForeignKey.CASCADE)],
    indices = [Index("MediaId")]
)
data class ImageDBModel(
    @ColumnInfo(name = "PostId", index = true)
    val postId: String,
    @PrimaryKey
    @ColumnInfo(name = "MediaId")
    val mediaId: String,
    @ColumnInfo(name = "Title")
    val title: String?,
    @ColumnInfo(name ="Description")
    val description: String?,
    @ColumnInfo(name = "DateTime")
    val datetime: Long,
    @ColumnInfo(name = "Mp4")
    val mp4: String?,
    @ColumnInfo(name = "Link")
    val link: String
)