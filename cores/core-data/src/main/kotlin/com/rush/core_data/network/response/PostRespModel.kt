package com.rush.core_data.network.response

import com.rush.core_data.room.model.ImageDBModel
import com.rush.core_data.room.model.JointPostDBModel
import com.rush.core_data.room.model.PostDBModel
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PostRespModel(
    @Json(name = "id")
    val id: String,
    @Json(name = "title")
    val title: String,
    @Json(name = "datetime")
    val datetime: Long,
    @Json(name = "cover")
    val cover: String?,
    @Json(name = "account_url")
    val owner: String,
    @Json(name = "views")
    val views: Int,
    @Json(name = "link")
    val link: String,
    @Json(name = "ups")
    val thumbUps: Int,
    @Json(name = "downs")
    val thumbDowns: Int,
    @Json(name = "comment_count")
    val commentCount: Int,
    @Json(name = "is_album")
    val isAlbum: Boolean,
    @Json(name = "images_count")
    val mediaCount: Int?,
    @Json(name = "tags")
    val tags: List<TagRespModel>,
    @Json(name = "images")
    val images: List<ImageRespModel>?
)

fun PostRespModel.toDB(postType: Int): JointPostDBModel {
    val post = PostDBModel(
        id = this.id,
        mediaId = this.id,
        title = this.title,
        datetime = this.datetime,
        cover = this.cover,
        owner = this.owner,
        views = this.views,
        link = this.link,
        thumbDowns = this.thumbDowns,
        thumbUps = this.thumbUps,
        commentCount = this.commentCount,
        isAlbum = this.isAlbum,
        mediaCount = this.mediaCount,
        postType = postType
    )
    var images: List<ImageDBModel> = listOf()
    this.images?.let {
        images = it.toDB(this.id)
    }
    val tag = this.tags.toDB()
    return JointPostDBModel(
        post = post,
        images = images,
        tags = tag
    )
}

fun List<PostRespModel>.toDB(postType: Int): List<JointPostDBModel> =
    this.map { it.toDB(postType) }