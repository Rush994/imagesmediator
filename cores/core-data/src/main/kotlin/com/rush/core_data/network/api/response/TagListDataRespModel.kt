package com.rush.core_data.network.api.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TagListDataRespModel(
    @Json(name = "data")
    val data: TagListRespModel,
    @Json(name = "success")
    val success: Boolean,
    @Json(name = "status")
    val status: Int
)