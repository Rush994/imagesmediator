package com.rush.core_data.di.module

import android.app.Application
import androidx.room.Room
import com.rush.core_data.di.CoreMainScope
import com.rush.core_data.room.AppDatabase
import com.rush.core_data.room.dao.*
import dagger.Module
import dagger.Provides

@Module
class RoomModule {
    @CoreMainScope
    @Provides
    fun providesRoomDatabase(application: Application): AppDatabase =
        Room.databaseBuilder(application, AppDatabase::class.java, AppDatabase.DATABASE_NAME).build()

    @CoreMainScope
    @Provides
    fun providePostsOuterBestDao(appDatabase: AppDatabase): PostsDao =
        appDatabase.getPostsOutersDao()

    @CoreMainScope
    @Provides
    fun providePostInnersBestDao(appDatabase: AppDatabase): ImagesDao =
        appDatabase.getPostInnersDao()

    @CoreMainScope
    @Provides
    fun provideCommentsBestDao(appDatabase: AppDatabase): CommentsDao =
        appDatabase.getCommentsDao()

    @CoreMainScope
    @Provides
    fun providePostTagsDao(appDatabase: AppDatabase): TagsDao =
        appDatabase.getPostTagsDao()

    @CoreMainScope
    @Provides
    fun providePostTagJoinsBestDao(appDatabase: AppDatabase): PostTagJoinsDao =
        appDatabase.getPostTagJoinsDao()
}