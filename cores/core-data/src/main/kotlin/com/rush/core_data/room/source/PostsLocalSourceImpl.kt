package com.rush.core_data.room.source

import com.rush.core_data.room.dao.*
import com.rush.core_data.room.model.*
import com.rush.core_presentation.type.CategoryTypes
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import javax.inject.Inject

class PostsLocalSourceImpl @Inject constructor(
    private val postsDao: PostsDao,
    private val tagsDao: TagsDao,
    private val postTagJoinsDao: PostTagJoinsDao,
    private val imagesDao: ImagesDao,
    private val commentsDao: CommentsDao
) : PostsLocalSource {
    override fun observePost(postId: String): Flowable<PostDBModel> {
        TODO("Not yet implemented")
    }

    override fun observePostsByType(postType: Int): Flowable<List<PostDBModel>> =
        postsDao.observePostsByType(postType)

    override fun observeTags(postId: String): Flowable<List<TagDBModel>> {
        TODO("Not yet implemented")
    }

    override fun observeComments(postId: String): Flowable<List<CommentDBModel>> {
        TODO("Not yet implemented")
    }

    override fun insertPosts(jointPosts: List<JointPostDBModel>): Completable =
        Completable.fromAction {
            val posts: MutableList<PostDBModel> = mutableListOf()
            val tags: MutableList<TagDBModel> = mutableListOf()
            val postTagJoins: MutableList<PostTagJoinDBModel> = mutableListOf()

            jointPosts.forEach {
                if (it.post.isAlbum) {
                    it.post.mediaId = it.post.cover!!
                }
                posts.add(it.post)
                tags.addAll(it.tags)

                for (item in it.tags)
                    postTagJoins.add(
                        PostTagJoinDBModel(
                            it.post.id,
                            0
                        )
                    )
            }
            postsDao.insertOrReplace(posts)
            val ids = tagsDao.insertOrReplace(tags)
            val map = postTagJoins.zip(ids).toList()
            postTagJoins.clear()
            for (item in map) {
                item.first.tagId = item.second
                postTagJoins.add(item.first)
            }
            postTagJoinsDao.insertOrReplace(postTagJoins)
            if ((jointPosts.size == 1) && (jointPosts[0].post.postType == CategoryTypes.Single.value))
                imagesDao.insertOrReplace(jointPosts[0].images)
        }

    override fun observeImages(postId: String): Flowable<List<ImageDBModel>> {
        TODO("Not yet implemented")
    }

    override fun insertImage(image: ImageDBModel): Completable {
        TODO("Not yet implemented")
    }

    override fun insertImages(jointPost: JointPostDBModel): Completable {
        TODO("Not yet implemented")
    }

    override fun insertComments(commentList: List<CommentDBModel>): Completable {
        TODO("Not yet implemented")
    }

    override fun deletePosts(postType: Int): Completable =
        Completable.fromAction {
            postsDao.deletePostList(postType)
        }

    override fun deletePostsByTag(): Completable {
        TODO("Not yet implemented")
    }

}