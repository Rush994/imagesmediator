package com.rush.core_data.network.api.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ErrorRespModel(
    @Json(name ="error")
    val error: String,
    @Json(name ="request")
    val request: String,
    @Json(name ="method")
    val method: String
)
