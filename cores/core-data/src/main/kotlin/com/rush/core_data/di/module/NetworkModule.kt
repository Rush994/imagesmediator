package com.rush.core_data.di.module

import com.rush.core_data.network.api.ImgurApi
import com.rush.core_data.network.interceptor.TokenInterceptor
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class NetworkModule {
    @com.rush.core_data.di.CoreMainScope
    @Provides
    fun provideRetrofit(tokenInterceptor: TokenInterceptor): Retrofit =
        ImgurApi.getApiInstance(tokenInterceptor)

    @com.rush.core_data.di.CoreMainScope
    @Provides
    fun provideApi(retrofit: Retrofit): ImgurApi =
        retrofit.create(ImgurApi::class.java)
}