package com.rush.core_data.room.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.rush.core_data.room.AppDatabase

@Entity(tableName = AppDatabase.TABLE_POST_TAGS)
data class TagDBModel(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Id")
    val id: Long = 0,
    @ColumnInfo(name = "Name")
    val name: String,
    @ColumnInfo(name = "DisplayName")
    val displayName: String,
    @ColumnInfo(name = "Followers")
    val followers: Int,
    @ColumnInfo(name = "TotalItems")
    val totalItems: Int
)