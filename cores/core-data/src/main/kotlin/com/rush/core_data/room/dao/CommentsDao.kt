package com.rush.core_data.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.rush.core_data.room.AppDatabase
import com.rush.core_data.room.model.CommentDBModel
import io.reactivex.rxjava3.core.Flowable

@Dao
abstract class CommentsDao: BaseDao<CommentDBModel>(AppDatabase.TABLE_COMMENTS) {
    @Query("SELECT * FROM Comments WHERE PostId LIKE :postId")
    abstract fun observeCommentList(postId: String): Flowable<List<CommentDBModel>>
}