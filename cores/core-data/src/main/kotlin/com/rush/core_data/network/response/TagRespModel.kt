package com.rush.core_data.network.response

import com.rush.core_data.room.model.TagDBModel
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TagRespModel(
    @Json(name = "name")
    val name: String,
    @Json(name = "display_name")
    val displayName: String,
    @Json(name = "followers")
    val followers: Int,
    @Json(name = "total_items")
    val totalItems: Int
)

fun TagRespModel.toDB() =
    TagDBModel(
        name = this.name,
        displayName = this.displayName,
        followers = this.followers,
        totalItems = this.totalItems
    )

fun List<TagRespModel>.toDB(): List<TagDBModel> =
    this.map { it.toDB() }
