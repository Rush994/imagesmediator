package com.rush.core_data.network.api.response

import com.rush.core_data.network.response.PostListByTagDataRespModel
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PostListByTagRespModel(
    @Json(name = "data")
    val data: PostListByTagDataRespModel,
    @Json(name ="success")
    val success: Boolean,
    @Json(name ="status")
    val status: Int
)
