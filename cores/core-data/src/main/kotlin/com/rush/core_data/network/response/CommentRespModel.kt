package com.rush.core_data.network.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CommentRespModel(
    @Json(name = "id")
    val id: Long,
    @Json(name = "image_id")
    val imageId: String,
    @Json(name = "comment")
    val comment: String,
    @Json(name = "author")
    val author: String,
    @Json(name = "ups")
    val ups: Long,
    @Json(name = "downs")
    val downs: Long,
    @Json(name = "platform")
    val platform: String
)