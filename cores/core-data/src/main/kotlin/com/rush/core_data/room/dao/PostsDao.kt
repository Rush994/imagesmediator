package com.rush.core_data.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.rush.core_data.room.AppDatabase
import com.rush.core_data.room.model.PostDBModel
import io.reactivex.rxjava3.core.Flowable

@Dao
abstract class PostsDao: BaseDao<PostDBModel>(AppDatabase.TABLE_POSTS) {
    @Query("SELECT * FROM Posts WHERE PostType=:postType")
    abstract fun observePostsByType(postType: Int): Flowable<List<PostDBModel>>

    @Query("SELECT * FROM Posts WHERE Id LIKE :postId")
    abstract fun findPost(postId: String): Flowable<PostDBModel>

    @Query("DELETE FROM Posts WHERE PostType=:postType")
    abstract fun deletePostList(postType: Int)
}