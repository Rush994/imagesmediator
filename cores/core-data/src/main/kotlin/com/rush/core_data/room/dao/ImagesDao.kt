package com.rush.core_data.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.rush.core_data.room.AppDatabase
import com.rush.core_data.room.model.ImageDBModel
import io.reactivex.rxjava3.core.Flowable

@Dao
abstract class ImagesDao: BaseDao<ImageDBModel>(AppDatabase.TABLE_IMAGES) {
    @Query("SELECT * FROM Images WHERE PostId LIKE :postId")
    abstract fun observePostSingle(postId: String): Flowable<List<ImageDBModel>>
}