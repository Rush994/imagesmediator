package com.rush.core_data.room.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.rush.core_data.room.AppDatabase
import com.rush.core_domain.entity.PostEntity
import com.rush.core_presentation.type.CategoryTypes
import javax.inject.Inject

@Entity(tableName = AppDatabase.TABLE_POSTS)
data class PostDBModel(
    @PrimaryKey
    @ColumnInfo(name = "Id", index = true)
    val id: String,
    @ColumnInfo(name = "ImageId")
    var mediaId: String,
    @ColumnInfo(name = "PostTitle")
    val title: String,
    @ColumnInfo(name = "DateTime")
    val datetime: Long,
    @ColumnInfo(name = "Cover")
    val cover: String?,
    @ColumnInfo(name = "Owner")
    val owner: String,
    @ColumnInfo(name = "Views")
    val views: Int,
    @ColumnInfo(name = "Link")
    val link: String,
    @ColumnInfo(name = "ThumbUps")
    val thumbUps: Int,
    @ColumnInfo(name = "ThumbDowns")
    val thumbDowns: Int,
    @ColumnInfo(name = "CommentCount")
    val commentCount: Int,
    @ColumnInfo(name = "IsAlbum")
    val isAlbum: Boolean,
    @ColumnInfo(name = "MediaCount")
    val mediaCount: Int?,
    @ColumnInfo(name = "PostType")
    val postType: Int
)

fun PostDBModel.toEntity(): PostEntity =
    PostEntity(
        id = this.id,
        mediaId = this.mediaId,
        title = this.title,
        datetime = this.datetime,
        cover = this.cover,
        owner = this.owner,
        views = this.views,
        link = this.link,
        thumbUps = this.thumbUps,
        thumbDowns = this.thumbDowns,
        commentCount = this.commentCount,
        isAlbum = this.isAlbum,
        mediaCount = this.mediaCount,
        categoryType = CategoryTypes.Default
    )

fun List<PostDBModel>.toEntity(): List<PostEntity> =
    this.map { it.toEntity() }