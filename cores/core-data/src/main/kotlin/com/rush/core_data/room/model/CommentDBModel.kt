package com.rush.core_data.room.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.rush.core_data.room.AppDatabase

@Entity(tableName = AppDatabase.TABLE_COMMENTS,
    foreignKeys = [
        ForeignKey(
            entity = PostDBModel::class,
            parentColumns = ["Id"],
            childColumns = ["PostId"],
            onDelete = ForeignKey.CASCADE)])
data class CommentDBModel(
    @PrimaryKey
    @ColumnInfo(name="Id", index = true)
    val id: Long,
    @ColumnInfo(name = "ImageId")
    val imageId: String,
    @ColumnInfo(name = "Comment")
    val comment: String,
    @ColumnInfo(name = "Author")
    val author: String,
    @ColumnInfo(name = "Ups")
    val ups: Long,
    @ColumnInfo(name = "Downs")
    val downs: Long,
    @ColumnInfo(name = "Platform")
    val platform: String,

    @ColumnInfo(name = "PostId", index = true)
    val postId: String
)