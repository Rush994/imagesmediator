package com.rush.core_data.room.model

data class JointPostDBModel(
    val post: PostDBModel,
    val images: List<ImageDBModel>,
    val tags: List<TagDBModel>
)