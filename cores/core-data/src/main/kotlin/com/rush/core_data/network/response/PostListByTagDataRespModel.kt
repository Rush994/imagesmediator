package com.rush.core_data.network.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PostListByTagDataRespModel(
    @Json(name = "name")
    val name: String,
    @Json(name = "display_name")
    val displayName: String,
    @Json(name = "followers")
    val followers: Int,
    @Json(name = "total_items")
    val totalItems: Int,
    @Json(name = "following")
    val following: Boolean,
    @Json(name = "is_whitelisted")
    val isWhitelisted: Boolean,
    @Json(name = "background_hash")
    val backgroundHash: String,
    @Json(name = "background_is_animated")
    val backgroundIsAnimated: Boolean,
    @Json(name = "items")
    val items: List<PostRespModel>
)
