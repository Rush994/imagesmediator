package com.rush.core_data.network.response

import com.rush.core_data.room.model.ImageDBModel
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ImageRespModel(
    @Json(name = "id")
    val id: String,
    @Json(name = "title")
    val title: String?,
    @Json(name = "description")
    val description: String?,
    @Json(name = "datetime")
    val datetime: Long,
    @Json(name = "mp4")
    val mp4: String?,
    @Json(name = "link")
    val link: String
)

fun ImageRespModel.toDB(postId: String): ImageDBModel =
    ImageDBModel(
        postId = postId,
        mediaId = this.id,
        title = this.title,
        description = this.description,
        datetime = this.datetime,
        mp4 = this.mp4,
        link = this.link
    )
fun List<ImageRespModel>.toDB(postId: String): List<ImageDBModel> =
    this.map { it.toDB(postId) }