package com.rush.core_data.room.source

import com.rush.core_data.room.model.*
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable

interface PostsLocalSource {
    fun observePost(postId: String): Flowable<PostDBModel>
    fun observePostsByType(postType: Int): Flowable<List<PostDBModel>>
    fun observeTags(postId: String): Flowable<List<TagDBModel>>
    fun observeComments(postId: String): Flowable<List<CommentDBModel>>
    fun insertPosts(jointPosts: List<JointPostDBModel>): Completable
    fun observeImages(postId: String): Flowable<List<ImageDBModel>>
    fun insertImage(image: ImageDBModel): Completable
    fun insertImages(jointPost: JointPostDBModel): Completable
    fun insertComments(commentList: List<CommentDBModel>): Completable
    fun deletePosts(postType: Int): Completable
    fun deletePostsByTag(): Completable
}