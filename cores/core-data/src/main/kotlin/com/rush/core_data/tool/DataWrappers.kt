package com.rush.core_data.tool

import com.rush.core_data.tool.ResultSealed.Failure
import com.rush.core_data.tool.ResultSealed.Success

sealed class ResultSealed<out T> {
    data class Success<T>(val value: T): ResultSealed<T>()
    data class Failure(val cause: Throwable): ResultSealed<Nothing>()
}

inline fun <T> ResultWrapper(block: () -> T): ResultSealed<T> = try {
    Success(block())
} catch (t: Throwable) {
    Failure(t)
}

fun <T> ResultSealed<T>.requireValue(): T = when (this) {
    is Success -> value
    is Failure -> throw cause
}