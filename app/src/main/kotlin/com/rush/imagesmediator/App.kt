package com.rush.imagesmediator

import android.app.Application
import com.rush.core_data.Core

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        //InjectAppComponent.build(this)
        Core.initialize(this)
    }
}