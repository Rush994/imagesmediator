package com.rush.imagesmediator

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.rush.container_main.ui.MainContainerActivity

class AppActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Intent(this, MainContainerActivity::class.java).also {
            intent.extras?.let { bundle -> it.putExtras(bundle) }
        }.also {
            startActivity(it)
        }
    }
}