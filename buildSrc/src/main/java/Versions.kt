object Versions {
    const val retrofit = "2.9.0"
    const val converterGson = "2.8.1"
    const val logging_interceptor = "4.9.1"
    const val moshi = "1.13.0"
    const val room = "2.3.0"

    const val rxJava = "3.0.0"
    const val rxAndroid = "3.0.0"
    const val rxKotlin = "3.0.0"

    const val dagger = "2.43.2"

    const val cicerone = "7.1"

    const val timber = "4.7.1"

    const val bottomnavigation = "2.2.0"
    const val swiperefresh = "1.1.0"

    const val coroutines = "1.5.0"

    const val lifecycle = "2.3.1"
    const val ktxCore = "1.6.0"
    const val appCompat = "1.3.1"
    const val constraint = "2.1.0"
    const val ktxLifecycleRuntime = "2.3.1"

    const val glide = "4.12.0"
    const val glideTransformations = "4.0.1"
}