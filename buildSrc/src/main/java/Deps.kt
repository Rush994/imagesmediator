object AndroidDeps{
    const val lifecycleViewmodel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
    const val ktxCore = "androidx.core:core-ktx:${Versions.ktxCore}"
    const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    const val constraint = "androidx.constraintlayout:constraintlayout:${Versions.constraint}"
    const val ktxLifecycleRuntime = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.ktxLifecycleRuntime}"
}

object LoggingDeps{
    const val timber = "com.jakewharton.timber:timber:${Versions.timber}"
}

object NetworkDeps{
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val converterGson = "com.squareup.retrofit2:converter-gson:${Versions.converterGson}"
    const val converterMoshi = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"
    const val moshi = "com.squareup.moshi:moshi:${Versions.moshi}"
    const val moshiCodegen = "com.squareup.moshi:moshi-kotlin-codegen:${Versions.moshi}"

    const val adapterRx = "com.squareup.retrofit2:adapter-rxjava3:${Versions.retrofit}"
    const val loggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.logging_interceptor}"
}

object RoomDeps{
    const val ktx = "androidx.room:room-ktx:${Versions.room}"
    const val runtime = "androidx.room:room-runtime:${Versions.room}"
    const val rxJava3 = "androidx.room:room-rxjava3:${Versions.room}"
    const val compiler = "androidx.room:room-compiler:${Versions.room}"
}

object DaggerDeps{
    const val dagger = "com.google.dagger:dagger:${Versions.dagger}"
    const val daggerCompiler = "com.google.dagger:dagger-compiler:${Versions.dagger}"
}

object NavigationDeps{
    const val cicerone = "com.github.terrakok:cicerone:${Versions.cicerone}"
}

object RxDeps{
    const val rxJava = "io.reactivex.rxjava3:rxjava:${Versions.rxJava}"
    const val rxAndroid = "io.reactivex.rxjava3:rxandroid:${Versions.rxAndroid}"
    const val rxKotlin = "io.reactivex.rxjava3:rxkotlin:${Versions.rxKotlin}"
}

object CoroutinesDeps{
    const val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"
}

object ContainerDeps{
    const val main = ":containers:container-main"
}

object FeatureDeps{
    const val containerPosts = ":features:feature-container-posts"
    const val postsTop = ":features:feature-posts-top"
    const val postsCat = ":features:feature-posts-cat"
    const val postsDog = ":features:feature-posts-dog"
    const val post = ":features:feature-post"
}

object CoreDeps{
    const val data = ":cores:core-data"
    const val presentation = ":cores:core-presentation"
    const val navigation = ":cores:core-navigation"
    const val domain = ":cores:core-domain"
}

object ViewDeps{
    const val snack = ":views:view-mediator-snackbar"
    const val postItem = ":views:view-post-list-item"
}

object HelperDeps{
    const val bottomnavigation = "com.ashokvarma.android:bottom-navigation-bar:${Versions.bottomnavigation}"
    const val swiperefresh = "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.swiperefresh}"
}

object GlideDeps {
    val runtime = "com.github.bumptech.glide:glide:${Versions.glide}"
    val compiler = "com.github.bumptech.glide:compiler:${Versions.glide}"
    val transformations = "jp.wasabeef:glide-transformations:${Versions.glideTransformations}"
}