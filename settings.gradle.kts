pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        maven {
            url = uri("https://jitpack.io")
        }
        google()
        mavenCentral()
    }
}
enableFeaturePreview("VERSION_CATALOGS")
rootProject.name = "ImagesMediator"

include(":app")
include(":containers:container-main")

//Cores
include(":cores:core-data")
include(":cores:core-domain")
include(":cores:core-presentation")
include(":cores:core-navigation")

//Features
include(":features:feature-container-posts")
include(":features:feature-posts-top")
include(":features:feature-posts-cat")
include(":features:feature-posts-dog")

//Views
include(":views:view-mediator-snackbar")
include(":views:view-post-list-item")
include(":features:feature-post")
